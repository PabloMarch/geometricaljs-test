import gulp from 'gulp';
import notify from 'gulp-notify';
import rename from 'gulp-rename';
import config from '../config';
// import config from './package.json';

export default function styles () {
  return gulp.src('./source/sass/main.scss')
    .pipe(rename({ suffix: `.${config.version}` }))
    .pipe(gulp.dest('./builds'))
    .pipe(notify({ message: 'TEST STYLES' }));
}
