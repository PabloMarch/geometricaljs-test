# Javascript Geometric Test
### dev[JPMA]

## Developer Setup
### Requirements.
1. Node.
2. Gulp.
3. Geek people.

### Installation
1. Install latest node version `npm i -g npm@latest`
2. Positioned on the repo's root, then run `npm install`
3. Run `gulp` or `gulp serve` to run the server on  *localhost:3000*
4. **You're good to start coding now**.

### Notes
- About the console warning 're-evaluating native module' please refers to https://github.com/gulpjs/gulp/issues/1571
