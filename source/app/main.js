import DocReady from 'es6-docready';
import App from './App';

DocReady(() => {
  const props = {
    dotsAllowed: 3
  }
  const app = new App(props);
  app.init();
})
