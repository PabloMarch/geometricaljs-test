  import paperScope, { view } from 'paper';
  import * as util from './Utils';

  export default class App {
    constructor (props) {
      this.canvas = document.getElementById('app');
      this.dotsCount = props.dotsAllowed;
      this.sceneCreated = false;
      this.points = [];
    }

    init () {
      paperScope.setup(this.canvas);
      this.eventHandlers();
    }

    createScene () {
      let generatedPoint = util.calculatePoint(this.points);
      this.points.push(generatedPoint);

      let figureCenter = util.getFigureCenter(this.points);
      let parallelArea = util.calculateParallelArea(this.points);
      let dot = {
        fillColor: '#FFC107',
        diameter: 7,
        point: figureCenter
      }

      this.sceneCreated = true;
      util.createCircle(dot);

      console.log('AREA:: ', parallelArea);
    }

    clearScene () {
      console.log('CLEAR SCENE');
    }

    onMouseDown = evt => {
      if (this.sceneCreated) return;

      let lastDot = this.points[this.points.length - 1];
      let dot = {
        fillColor: '#f00',
        diameter: 11
      }

      Object.assign(dot, evt);
      util.createCircle(dot);
      this.points.push(evt.point);

      if (this.points.length >= 2) util.tracePath(lastDot, dot.point);
      if (this.points.length >= this.dotsCount) this.createScene();
    }

    eventHandlers () {
      view.onMouseDown = e => this.onMouseDown(e);
    }
  }
