import { Path } from 'paper';

export function createCircle (config) {
  const { point, diameter, fillColor } = config;

  let path = new Path.Circle({
    center: point,
    radius: diameter / 2,
    fillColor: fillColor
  });

  return path;
}

export function tracePath (startPoint, endPoint) {
  let path = new Path({
    segments: [[startPoint.x, startPoint.y], [endPoint.x, endPoint.y]]
  });

  path.strokeColor = '#2196F3';
  path.strokeWidth = 2;
  path.strokeCap = 'round';
  path.dashArray = [10, 12];
  return path;
}

export function calculatePoint (points) {
  let dot = {
    fillColor: '#00f',
    diameter: 11,
    point: {
      x: (points[0].x + points[2].x - points[1].x),
      y: (points[0].y + points[2].y - points[1].y)
    }
  }

  createCircle(dot);
  tracePath(points[0], dot.point);
  tracePath(points[2], dot.point);

  return dot.point;
}

export function calculateParallelArea (points) {
  let base = tracePath(points[0], points[1]).length;
  let height = tracePath(points[0], points[3]).length * Math.sin(points[0].angle);
  return base * height;
}

export function getFigureCenter (points) {
  return {
    x: (points[0].x + points[2].x)/2,
    y: (points[1].y + points[3].y)/2,
  }
}
