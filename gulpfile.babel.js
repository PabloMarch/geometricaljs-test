'use strict';

// ### Packages
import gulp from 'gulp';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sourcemaps from 'gulp-sourcemaps';
import del from 'del';
import notify from 'gulp-notify';
import serve from 'browser-sync';
import eslint from 'gulp-eslint';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import { babel, dependencies } from './package.json';
import styles from './gulp/tasks/styles';

// ### Config Variables
const sourcePath = './source';
const buildPath  = './builds';

const config = {
  paths: {
    app: sourcePath + '/app',
    dev: buildPath + '/dev',
    prod: buildPath + '/prod',
    dependencies: Object.keys(dependencies || {})
  }
};

// Enviroment @TODO: Update once enviropments are setup
config.env = config.paths.dev;
const browsersync = serve.create();


// ### Move files to final destination
gulp.task('movefiles', () => {
  return gulp.src([`${sourcePath}/**/*.html`])
    .pipe(gulp.dest(config.env))
    .pipe(notify({ message : 'html files copied.' }));
});

gulp.task('styles', styles);


// ### Build
gulp.task('scripts', [/*'eslint'*/], () => {
  let bundle = browserify({
      entries: `${config.paths.app}/main.js`,
      debug: true
    })
    .transform('babelify', babel);

  return bundle.bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify({mangle: false}))
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('./_sourcemaps'))
    .pipe(gulp.dest(config.env))
    .pipe(notify({ message : 'Scripts compiled.' }))
    .pipe(browsersync.stream());
});


// ### Lint
gulp.task('eslint', () => {
  return gulp.src(`${config.paths.app}/**/*.js`)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(notify({ message : 'Running eslint.' }));
});


// ### Clean
// `gulp clean` - Deletes the build folder entirely.
gulp.task('clean', () => del([buildPath]));


/// ### Build
gulp.task('build', ['scripts', 'movefiles']);


// ### Watch
gulp.task('watch', ['build'], () => {
  browsersync.init({ server : config.env });

  gulp.watch(`${sourcePath}/**/*.js`, ['scripts']);
  gulp.watch(`${sourcePath}/**/*.html`, ['movefiles']).on('change', browsersync.reload);
});


// ### Defaults
gulp.task('default', ['clean', 'build']);
